__author__ = 'arhangel66'
# -*- coding: utf-8 -*-

from django.contrib import admin
from order.models import Status, StatusHistory, CatalogUslug, Order, OrderType


class StatusAdmin(admin.ModelAdmin):
    list_display = ('name', )
    list_filter = ('types', )

class CatalogUslugAdmin(admin.ModelAdmin):
    list_display = ('name', )


class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_full_fio', 'sum_sum', 'status')
    search_fields = ('id', 'phone', )
    list_filter = ('type', )


class StatusHistoryAdmin(admin.ModelAdmin):
    list_display = ('status', 'order', 'date')
    list_filter = ('status', )
    search_fields = ('order', )


class SmsHistoryAdmin(admin.ModelAdmin):
    list_display = ('order', 'status', 'send', 'date')
    list_filter = ('status', )
    search_fields = ('order', 'send', 'master')


class OrderTypeAdmin(admin.ModelAdmin):
    list_display = ('name', )


admin.site.register(OrderType, OrderTypeAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Status, StatusAdmin)
admin.site.register(CatalogUslug, CatalogUslugAdmin)
admin.site.register(StatusHistory, StatusHistoryAdmin)
