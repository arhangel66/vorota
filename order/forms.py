# -- coding: utf-8 --
__author__ = 'arhangel66'
from django import forms
from order.models import Order, OrderRow, Status, CatalogUslug, OrderType
from persons.models import Person
from persons.tools import fio_obrab
from order.tools import create_client_from_order, save_status_to_history
from django.db.models import Q


class OrderForm(forms.ModelForm):
    """
    Форма заполнения данных при покупке
    """
    id = forms.CharField(max_length=5, required=False, widget=forms.TextInput(attrs={'style': 'display:none'}))
    fio_full = forms.CharField(max_length=200, required=False, widget=forms.TextInput(attrs={'placeholder': "Фамилия Имя Отчество", 'class': 'span4'}))
    orgname = forms.CharField(max_length=200, required=False, widget=forms.TextInput(attrs={'placeholder': "Название организации", 'class': 'span4'}))
    phone = forms.CharField(max_length=30, required=False, widget=forms.TextInput(attrs={'placeholder': "Телефон", 'class': 'span4'}))
    email = forms.CharField(max_length=50, required=False, widget=forms.TextInput(attrs={'placeholder': "Email", 'class': 'span4'}))
    city = forms.CharField(max_length=50, required=False, widget=forms.TextInput(attrs={'placeholder': "Город", 'class': 'span4'}))
    subcity = forms.CharField(max_length=50, required=False, widget=forms.TextInput(attrs={'placeholder': "Населенный пункт", 'class': 'span4'}))
    street = forms.CharField(max_length=50, required=False, widget=forms.TextInput(attrs={'placeholder': "Улица", 'class': 'span4'}))
    home = forms.CharField(max_length=20, required=False, widget=forms.TextInput(attrs={'placeholder': "Дом", 'class': 'span3'}))
    apt = forms.CharField(max_length=5, required=False, widget=forms.TextInput(attrs={'placeholder': "Квартира", 'class': 'span1'}))
    type = forms.ModelChoiceField(OrderType.objects.all(), widget=forms.Select(attrs={'class': 'span4'}), required=False)
    comment = forms.CharField(widget=forms.Textarea(attrs={'cols': '50', 'rows': '4', 'class': 'span4'}), required=False)
    master = forms.ModelChoiceField(Person.objects.filter(master=True, active=True), widget=forms.Select(attrs={'class': 'span4'}), required=False)
    status = forms.ModelChoiceField(Status.objects.all(), widget=forms.Select(attrs={'class': 'span4'}), required=False)

    def __init__(self, *args, **kwargs):
        order = kwargs.get('instance')

        super(OrderForm, self).__init__(*args, **kwargs)

        if order:
            self.initial["fio_full"] = order.get_full_fio()
            self.initial["orgname"] = order.orgname
            self.initial["status"] = order.status
            self.initial["phone"] = order.phone

        if not order or (order and not order.id):
            type = OrderType.objects.all()
            if type:
                self.initial["type"] = type[0]

    class Meta:
        model = Order
        exclude = ('client', 'creater', 'sum_sum', 'sum_mat', 'sum_trans', 'sum_zp', 'status')

    def clean(self):
        super(OrderForm, self).clean()
        return self.cleaned_data

    def save(self, order):
        data = self.cleaned_data
        fio1, fio2, fio3 = fio_obrab(data['fio_full'])
        if fio1:
            order.fio1 = fio1
        else:
            order.fio1 = ''

        if fio2:
            order.fio2 = fio2
        else:
            order.fio2 = ''

        if fio3:
            order.fio3 = fio3
        else:
            order.fio3 = ''

        order.type = data['type']
        order.phone = data['phone']
        order.orgname = data['orgname']
        order.email = data['email']
        order.city = data['city']
        order.subcity = data['subcity']
        order.street = data['street']
        order.home = data['home']
        order.apt = data['apt']
        order.comment = data['comment']
        order.client = create_client_from_order(order)
        if order.status != data['status'] or order.master != data['master']:
            order.status = data['status']
            order.master = data['master']
            save_status_to_history(data['status'], order)
        order.save()

        return order


class OrderRowForm(forms.ModelForm):
    """
    Форма услуги
    """
    id = forms.IntegerField(widget=forms.TextInput(attrs={'style': 'display:none'}), required=False)
    order = forms.ModelChoiceField(queryset=Order.objects.all(), widget=forms.TextInput(attrs={'style': 'display:none'}), required=False)
    item = forms.ModelChoiceField(queryset=CatalogUslug.objects.all(), required=False)

    price_sum = forms.IntegerField(label="Сумма", required=False, widget=forms.TextInput(attrs={'placeholder': "Сумма", "class": "input-mini"}))
    price_mat = forms.IntegerField(label="Мат", required=False, widget=forms.TextInput(attrs={'placeholder': "Материалы", 'class': 'input-mini'}))
    price_trans = forms.IntegerField(label="Транс", required=False, widget=forms.TextInput(attrs={'placeholder': "Транспортные", "class": "input-mini"}))
    price_zp = forms.IntegerField(label="Зп", required=False, widget=forms.TextInput(attrs={'placeholder': "з/п", "class": "input-mini"}))
    comment = forms.CharField(max_length=250, required=False, widget=forms.Textarea(attrs={'cols': '100', 'rows': '2', 'class': "span4"}))


    def __init__(self, *args, **kwargs):
        row = kwargs.get('instance')
        super(OrderRowForm, self).__init__(*args, **kwargs)
        if row:
            self.initial["id"] = row.id
            if CatalogUslug.objects.filter(name=row.item):
                self.initial["item"] = CatalogUslug.objects.filter(name=row.item)[0]
            if row.order_id:
                self.initial["order"] = row.order_id

    class Meta:
        model = OrderRow
        exclude = ()

    def clean(self):
        super(OrderRowForm, self).clean()
        return self.cleaned_data

    def save(self, row=False):
        data = self.cleaned_data
        row.item = data.get('item', 0).name
        row.price_sum = data.get('price_sum', 0)
        row.price_mat = data.get('price_mat', 0)
        row.price_trans = data.get('price_trans', 0)
        row.price_zp = data.get('price_zp', 0)
        row.comment = data['comment']
        row.order = data['order']
        row.save()
        print data

        return row