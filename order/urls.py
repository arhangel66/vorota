__author__ = 'arhangel66'
# -*- coding: utf-8 -*-
from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('order.views',
    # Examples:
    #url(r'^pool/(?P<idpool>\d+)/(?P<iduser>\w+)/(?P<idorder>\d+)/$', 'after_order', name="after_order",),
    url(r'^add/$', 'order_add', name="order_add",),
    url(r'^edit/(?P<order_id>\d+)/$', 'order_add', name="order_edit",),
    url(r'^list/$', 'order_list', name="order_list",),
    url(r'^list/(?P<full>\w+)/$', 'order_list', name="order_list",),
    url(r'^order_save/$', 'order_save', name="order_save",),

    url(r'^add_client/(?P<order_id>\d+)/(?P<client_id>\d+)/$', 'add_client_to_order', name="add_client_to_order",),
    url(r'^ajax/row_add/$', 'row_add', name="row_add",),
    url(r'^ajax/row_save/$', 'row_save', name="row_save",),
    url(r'^ajax/row_del/$', 'row_del', name="row_del",),
    url(r'^am/(?P<order_id>\d+)/(?P<master_id>\d+)/$', 'add_master_to_order', name="add_master_to_order",),
    )