# -- coding: utf-8 --
from django.db.models import Q

__author__ = 'arhangel66'
from persons.models import Person
from order.models import Order, OrderRow, Status, StatusHistory
from persons.tools import tel_obrab
from django.conf import settings
from django.core.urlresolvers import reverse


def get_text(text):
    try:
        text = str(text)
    except:
        text = str(text.encode('utf-8'))
    return text


def string_to_int(text):
    """
    Превращает текст в int
    """
    import re

    intval = 0
    text = re.sub("[^0-9.,\-]", "", get_text(text).strip().replace(',', '.'))
    if text:
        # print '51', text, type(text)
        intval = int(round(float(text)))
        # print '53', intval, type(intval)

    return intval


def create_client_from_order(order):
    """
    Создаю клиента из заказа
    """
    # Если к заказу уже прикреплен клиент - не создаю
    if order.client:
        return order.client

    # Если такой клиент уже есть, то не создаю, а просто выбираю его
    if order.phone and order.fio1:
        persons = Person.objects.filter(fio1=order.fio1, phone=order.phone)
        if persons:
            return persons[0]

    # И только если нет - создаю нового клиента
    person = Person()
    if order.fio1:
        person.fio1 = order.fio1

    if order.fio2:
        person.fio2 = order.fio2

    if order.fio3:
        person.fio3 = order.fio3

    person.orgname = order.orgname
    person.phone = tel_obrab(order.phone)
    person.save()
    return person

    # Возвращаю объект клиента
    pass


def search_orders_func(date1=None, date2=None, master_id=None, finish=False, statuses=['any'], type_id=None):
    orders = Order.objects.filter(~Q(fio1='', fio2='', fio3='', phone__isnull=True)).order_by('-id')
    FINISH_STATUS_ID = settings.FINISH_STATUS_ID
    if date1 and date2 is not None:
        orders = orders.filter(date__range=(date1, date2))

    if master_id and master_id != 'any':
        orders = orders.filter(master=master_id)

    if not 'any' in statuses:
        orders = orders.filter(status__in=statuses)

    if finish:
        orders = orders.filter(status=FINISH_STATUS_ID)

    if type_id and type_id != 'any':
        orders = orders.filter(type=type_id)

    return orders


def clear_all():
    Order.objects.all().delete()
    Person.objects.all().delete()


def create_new_demo():
    status1 = Status.objects.create(name="Заказ принят")
    status2 = Status.objects.create(name="Мастер назначен")
    status3 = Status.objects.create(name="Заказ завершен")
    status4 = Status.objects.create(name="Заказ отменен")

    client = Person.objects.create(fio1="Дербичев", fio2=u"Михаил", fio3=u"Борисович", phone="79090706997", street='Чичерина', home='25а', apt='63')
    client2 = Person.objects.create(fio1="Васильев", fio2=u"Иван", fio3=u"Васильевич", phone="79512345678")

    master = Person.objects.create(fio1="Мастер", fio2=u"Иван", fio3=u"Николаевич", phone="79517382506", master=True, active=True)
    master2 = Person.objects.create(fio1="Мастер2", fio2=u"Сергей", fio3=u"Борисович", phone="79222123456", master=True, active=True)

    order = Order.objects.create(fio1=u"Дербичев", fio2=u"Михаил", fio3=u"Борисович", phone='79090706997', client=client, master=master2)
    order2 = Order.objects.create(fio1=u"Дербичев", fio2=u"Михаил", fio3=u"Борисович", phone='79090706997', client=client, master=master2)
    OrderRow.objects.create(order_id=order.id, item=1, price_sum=3000, price_mat=500, price_zp=500, price_trans=500)
    OrderRow.objects.create(order_id=order.id, item=2, price_sum=5000, price_mat=1000, price_zp=500, price_trans=500)
    OrderRow.objects.create(order_id=order2.id, item=2, price_sum=5000, price_mat=1000, price_zp=500, price_trans=500)

    order3 = Order.objects.create(fio1=u"Васильев", fio2=u"Иван", fio3=u"Васильевич",  phone="79512345678", master=master, client=client2)
    order3.date = '2013-11-01'
    order3.save()
    OrderRow.objects.create(order_id=order3.id, item=1, price_sum=3000, price_mat=500, price_zp=500, price_trans=500)

    order4 = Order.objects.create(fio1=u"Васильев", fio2=u"123", fio3=u"123", phone='1231231', master_id=2, client=client2)

    order.calc_sum()
    order2.calc_sum()
    order3.calc_sum()
    order4.calc_sum()


def calc_sum_of_orders(orders, type='client'):
    """
    Считает сумму заказов и количество заказов, создавая словарь от person_id
    """
    # Словарь для хранения
    person_sum_count = {}

    # Обхожу все заказы, считая сумму и количество
    for order in orders:
        if type == 'master' and order.master:
            id = order.master.id
        elif order.client:
            id = order.client.id
        else:
            return {}
        person_sum_count.setdefault(id, {'count': 0, 'sum_sum': 0, 'last_date': order.date})
        person_sum_count[id]['count'] += 1
        if order.date > person_sum_count[id]['last_date']:
            person_sum_count[id]['last_date'] = order.date
        person_sum_count[id]['sum_sum'] += order.sum_sum if order.sum_sum else 0

    return person_sum_count


def send_sms(text, phone, name="73512227474", debug=False):
    """
    Отправляем СМС
    параметры:
    text - текст смс
    phone - номер получателя
    name - имя отправителя или обратный телефон
    """
    import urllib, urllib2
    #TODO: Расширить до работы с временем отправки
    values = {
            "login": settings.SMS_SERVER["login"],
            "pass": settings.SMS_SERVER["passw"],
            "from": name,
            "to": phone,
            "text": text.encode('utf-8'),
            }
    if debug:
        print '128', values
    url = u"https://www.smsdirect.ru/submit_message"
    data = urllib.urlencode(values)
    if debug:
        print '131', data
    req = urllib2.Request(url, data)
    try:
        response = urllib2.urlopen(req)
    except:
        print 'error'
        return False
    if debug:
        print '134', response
    return response.read()


def save_status_to_history(status, order):
    """
    Сохраняет статус в хистори, если там такого ещё нет.
    """
    status_history = StatusHistory.objects.create(status_id=status.id, order_id=order.id)
    count_send = 0
    if status_history.status.sms_person and StatusHistory.objects.filter(status=status.id, order=order.id).count() == 1:
        text_sms = create_sms_text(status_history.status.sms_person, order)
        # print '155', text_sms
        status_history.sms_client = text_sms

        count_send += 1 if send_sms(text=text_sms, phone=order.client.get_phone()) else 0

    if status_history.status.sms_master:
        text_sms = create_sms_text(status_history.status.sms_master, order)
        # print '161', text_sms
        status_history.sms_master = text_sms

        # Если мастер есть - шлем ему. Если нет - шлем всем мастерам.
        if order.master:
            count_send += 1 if send_sms(text=text_sms, phone=order.master.phone) else 0
        else:
            for master in Person.objects.filter(master=True, active=True):
                count_send += 1 if send_sms(text=text_sms, phone=master.phone) else 0
    status_history.count_send = count_send
    status_history.save()


def mass_replace(template, replace_dict):
    for key, val in replace_dict.items():
        if key in template:
            template = template.replace(key, "%s" % val)
    return template


def create_sms_text(template, order):
    add_master_url = ''
    master_phone = ''
    m_name = ''
    m_fname = ''
    street = order.street if order.street else ''
    home = order.home if order.home else ''
    if order.master:
        add_master_url = "http://%s%s" % (settings.HOST, reverse('order:add_master_to_order', args=[order.id, order.master.id]))
        master_phone = order.master.phone
        m_name = order.master.fio2
        m_fname = order.master.fio1
    kl_name = order.get_main_name()

    replace_dict = {
        "__master_fname__": m_fname,
        "__master_name__": m_name,
        "__kl_name__": kl_name,
        "__comment__": order.comment[:100] if order.comment else '',
        "__master_phone__": master_phone,
        "__add_master__": add_master_url,
        "__order_num__": order.id,
        "__order_sum__": order.sum_sum,
        "__2__": u"ул." + street + u" д." + home,
        "__3__": order.phone
    }
    sms_text = mass_replace(template, replace_dict)
    return sms_text

