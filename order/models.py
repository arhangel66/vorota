__author__ = 'arhangel66'
# -*- coding: utf-8 -*-

from django.db import models
from persons.models import Person
from django.contrib.auth.models import User
from django.db.models.signals import post_save
import datetime

CATALOG_ITEMS = (
    (1, u"бытовые гаражные ворота"),
    (2, u"автоматика"),
    (3, u"распашные ворота"),
    (4, u"откатные ворота"),
    (5, u"маркизы"),
    (6, u"шлагбаумы"),
    (7, u"рольставни"),
    (8, u"другое"),
    (9, u"промышленные секционные ворота"),
)


class CatalogUslug(models.Model):
    """
    Простейший каталог услуг
    """
    name = models.CharField(verbose_name="Название услуги", max_length=100, default='', blank=True)

    def __unicode__(self):
        return u"%s" % self.name

    class Meta:
        verbose_name = "Услуга"
        verbose_name_plural = "Услуги"


class OrderType(models.Model):
    """
    Тип заказов
    """
    name = models.CharField(verbose_name="Тип заказа", max_length=100, blank=True, null=True)

    class Meta:
        verbose_name = "тип заказа"
        verbose_name_plural = "Типы заказов"

    def __unicode__(self):
        return u"%s" % self.name


class Order(models.Model):
    """
    Заказ
    """
    fio1 = models.CharField(verbose_name="Фамилия", max_length=60, blank=True, null=True, default='')
    fio2 = models.CharField(verbose_name="Имя", max_length=60, blank=True, null=True, default='')
    fio3 = models.CharField(verbose_name="Отчество", max_length=60, blank=True, null=True, default='')
    orgname = models.CharField(verbose_name="Предприятие", max_length=100, blank=True, null=True, default='')
    date = models.DateTimeField(verbose_name="Дата создания", auto_now_add=True,)
    phone = models.CharField(verbose_name="Телефон", max_length=100, blank=True, null=True)
    email = models.CharField(verbose_name="Email", max_length=30, blank=True, null=True)
    client = models.ForeignKey(Person, verbose_name="Клиент", blank=True, null=True, related_name='client')
    master = models.ForeignKey(Person, verbose_name="Мастер", blank=True, null=True, related_name='master_order')
    creater = models.ForeignKey(User, verbose_name="Создатель заказа", blank=True, null=True)
    city = models.CharField(verbose_name="Город", max_length=60, blank=True, null=True)
    subcity = models.CharField(verbose_name="Населенный пункт", max_length=60, blank=True, null=True, default='')
    street = models.CharField(verbose_name="Улица", max_length=160, blank=True, null=True)
    home = models.CharField(verbose_name="Дом", max_length=60, blank=True, null=True)
    apt = models.CharField(verbose_name="Квартира", max_length=60, blank=True, null=True)
    sum_sum = models.IntegerField(verbose_name="Сумма заказа",  blank=True, null=True, default=0)
    sum_mat = models.IntegerField(verbose_name="Сумма материалов",  blank=True, null=True, default=0)
    sum_trans = models.IntegerField(verbose_name="Сумма расход",  blank=True, null=True, default=0)
    sum_zp = models.IntegerField(verbose_name="Сумма заказа",  blank=True, null=True, default=0)
    comment = models.CharField(verbose_name="Комментарий", max_length=255, blank=True, null=True)
    status = models.ForeignKey('Status', verbose_name="Статус", blank=True, null=True)
    type = models.ForeignKey(OrderType, verbose_name=u"Тип заказа", blank=True, null=True)

    def __unicode__(self):
        return u"Заказ №%s на %s" % (self.id, self.fio1)

    class Meta:
        verbose_name = "Заказ"
        verbose_name_plural = "Заказы"

    def calc_sum(self):
        """
        Вызывает пересчет всех сумм заказа исходя из ранее внесенных orders_row
        """
        rows = OrderRow.objects.filter(order=self.id)

        self.sum_sum = 0
        self.sum_mat = 0
        self.sum_trans = 0
        self.sum_zp = 0
        for row in rows:
            self.sum_sum += row.price_sum if row.price_sum else 0
            self.sum_mat += row.price_mat if row.price_mat else 0
            self.sum_trans += row.price_trans if row.price_trans else 0
            self.sum_zp += row.price_zp if row.price_zp else 0
        self.save()

    def get_full_fio(self):
        if self.fio1 or self.fio2 or self.fio3:
            full_fio = ''
            for name in [self.fio1, self.fio2, self.fio3]:
                if name:
                    full_fio += "%s " % name
            return full_fio
        else:
            return ''

    def is_red(self):
        if self.type_id == 2:
            return False

        STATUS_CANSEL_ID = 4
        if not self.master and (datetime.datetime.now() - self.date) > datetime.timedelta(minutes=30):
            if self.status and self.status.id == STATUS_CANSEL_ID:
                return False
            return True
        return False

    def get_adr(self):
        """
        Возвращаю адрес заказа
        """
        adr = ''
        for name in ['г.'+self.city, 'ул. '+self.street, 'д.'+self.home, 'кв.'+self.apt]:
            if name:
                adr += name
        return adr

    def get_main_name(self):
        if self.fio1:
            return self.fio1
        elif self.fio2:
            return self.fio2
        else:
            return self.fio3

    def get_pribil(self):
        pass


class OrderRow(models.Model):
    """
    Элемент заказа
    """
    order = models.ForeignKey(Order, verbose_name="Заказ", )
    item = models.CharField(max_length=100, verbose_name="Тип услуги", blank=True, null=True)
    price_sum = models.IntegerField(verbose_name="", blank=True, null=True, default=0)
    price_mat = models.IntegerField(verbose_name="", blank=True, null=True, default=0)
    price_trans = models.IntegerField(verbose_name="", blank=True, null=True, default=0)
    price_zp = models.IntegerField(verbose_name="", blank=True, null=True, default=0)
    comment = models.CharField(verbose_name="Комментарий", max_length=255, blank=True, null=True)


class Status(models.Model):
    """
    Статус заказа
    """
    name = models.CharField(verbose_name="Название статуса", max_length=60, blank=True, null=True)
    sms_person = models.CharField(verbose_name="Смс клиенту", blank=True, null=True, max_length=255,
                                  help_text="__master_name__ - Имя мастера,"
                                            "__master_fname__ - Фамилия мастера,"
                                            " __master_phone__ - телефон мастера, __add_master__ - ссылка на добавление мастера,"
                                            " __order_num__ - Номер заказа, __order_sum__ - сумма заказа"
                                            "\n__2__ - улица и дом, __3__ - телефон клиента\n __comment__ - Коммент \n __kl_name__ - Фамилия или имя клиента")
    sms_master = models.CharField(verbose_name="Смс мастеру", blank=True, null=True, max_length=255, help_text="__master_name__ - Имя мастера, __master_phone__ - телефон мастера, __add_master__ - ссылка на добавление мастера, __order_num__ - Номер заказа, __order_sum__ - сумма заказа"
                                                                                                               "\n__2__ - улица и дом, __3__ - телефон клиента\n __comment__ - Коммент \n __kl_name__ - Фамилия или имя клиента")
    email = models.TextField(verbose_name="Текст емайла", max_length=60, blank=True, null=True)
    types = models.ManyToManyField(OrderType, verbose_name=u"Типы заказов", blank=True, null=True)

    def __unicode__(self):
        return u"%s" % self.name

    class Meta:
        verbose_name = "Статус"
        verbose_name_plural = "Статусы"


class StatusHistory(models.Model):
    """
    История статусов
    """
    status = models.ForeignKey(Status, verbose_name="Статус")
    order = models.ForeignKey(Order, verbose_name="Заказ")
    date = models.DateTimeField(verbose_name="Дата статуса", auto_now_add=True)
    sms_client = models.CharField(verbose_name="Смс, отправленное клиенту", max_length=255, null=True, blank=True, default=None)
    sms_master = models.CharField(verbose_name="Смс, отправленное мастеру", max_length=255, null=True, blank=True, default=None)
    count_send = models.IntegerField(blank=True, null=True, verbose_name="Реально отправлено", default=0)

    def __unicode__(self):
        return u"%s - %s" % (self.status, self.date)

    class Meta:
        verbose_name = "История статусов"
        verbose_name_plural = "История статусов"


def auto_create_sms(sender, **kwargs):
    """
    Прописываю дату итд при сохранении
    """
    from order.tools import create_sms_text
    if "instance" in kwargs:
        status_history = kwargs["instance"]


post_save.connect(auto_create_sms, sender=StatusHistory)