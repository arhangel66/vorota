# -- coding: utf-8 --
from django.db.models import Q
from django.http import HttpResponseRedirect, Http404, HttpResponse,HttpResponseNotFound
from django.shortcuts import render_to_response,RequestContext, get_object_or_404
from avito_grab.models import Rules, AvitoAds
import datetime
from persons.models import Person
from order.models import Order, OrderRow, Status, OrderType
from django.core.urlresolvers import reverse
from order.forms import OrderForm, OrderRowForm
from order.tools import search_orders_func, string_to_int
from django.contrib.auth.decorators import login_required


def order_add(request, order_id=False, dop='good'):
    """
    Добавить заказ
    """
    if not order_id:
        order = Order()

    else:
        order = get_object_or_404(Order, pk=order_id)
    rows = OrderRow.objects.filter(order=order.id)

    for row in rows:
        row.item_name = row.item if row.item else ''

    form = OrderForm(instance=order)
    if order.type:
        form.fields['master'].queryset = Person.objects.filter(Q(master=True, active=True), Q(types=order.type_id) | Q(types__isnull=True))
        form.fields['status'].queryset = Status.objects.filter(Q(types=order.type_id) | Q(types__isnull=True))


    if request.POST:
        form = OrderForm(request.POST)
        if form.is_valid():
            order.save()
            form.save(order)
            return HttpResponseRedirect(reverse('order:order_edit', args=[order.id]))
    return render_to_response('order/order_add.html', locals(), context_instance=RequestContext(request))


@login_required
def order_list(request, dop='good', full=False):
    """
    Добавить заказ
    """
    show = request.POST.get('show', 'any')
    if not ',' in request.POST.get('show', ''):
        statuses = [request.POST.get('show', 'any')]
    else:
        statuses = request.POST.get('show', 'any').split(',')
    dat1 = request.POST.get('date1') if request.POST.get('date1') else (datetime.datetime.now() + datetime.timedelta(days=-50)).strftime("%Y-%m-%d")
    dat2 = request.POST.get('date2') if request.POST.get('date2') else (datetime.datetime.now() + datetime.timedelta(days=+1)).strftime("%Y-%m-%d")
    post_type = string_to_int(request.POST.get('type'))
    post_master = string_to_int(request.POST.get('master'))

    orders = search_orders_func(date1=dat1,
                                date2=dat2,
                                master_id=request.POST.get('master'),
                                finish=request.POST.get('finish'),
                                statuses=statuses,
                                type_id=post_type
                                )
    orders = orders.order_by('-pk')
    masters = Person.objects.filter(master=True, active=True)
    types = OrderType.objects.all()
    sum_sum = 0
    sum_mat = 0
    sum_zp = 0
    sum_trans = 0
    show_stats = [
        {'id': '3', 'name': "Только завершенные"},
        {'id': '1,2,4', 'name': "Скрыть завершенные"},
        {'id': '1,2,3', 'name': "Скрыть отмененные"},
        {'id': '1,2', 'name': "Только в работе"}
    ]


    for order in orders:
        order.id = (4-len(str(order.id)))*'0'+str(order.id)
        sum_sum += order.sum_sum if order.sum_sum else 0
        sum_mat += order.sum_mat if order.sum_mat else 0
        sum_zp += order.sum_zp if order.sum_zp else 0
        sum_trans += order.sum_trans if order.sum_trans else 0
    pribil = sum_sum - sum_mat - sum_zp - sum_trans
    print orders.values_list('id', flat=True)

    return render_to_response('order/order_list.html', locals(), context_instance=RequestContext(request))


def add_client_to_order(request, order_id, client_id):
    """
    Привязываем пользователя к заказу
    """
    client = get_object_or_404(Person, pk=client_id)
    order = get_object_or_404(Order, pk=order_id)
    order.fio1 = client.fio1
    order.fio2 = client.fio2
    order.fio3 = client.fio3
    order.phone = client.get_phone()
    order.orgname = client.orgname
    order.email = client.email
    order.city = client.city if not order.city else order.city
    order.street = client.street if not order.street else order.street
    order.home = client.home if not order.home else order.home
    order.apt = client.apt if not order.apt else order.apt
    order.client_id = client.id
    order.save()

    return HttpResponseRedirect(reverse('order:order_edit', args=[order_id]))


def row_save(request):
    """
    Сохранение услуги
    """
    if request.POST:
        if request.POST.get('id'):
            row = get_object_or_404(OrderRow, pk=request.POST.get('id'))
        else:
            row = OrderRow()

        form = OrderRowForm(request.POST, row)

        if form.is_valid():
            row = form.save(row)
        row.order.calc_sum()
        row.order.save()
    return HttpResponseRedirect(reverse("order:order_edit", args=[row.order.id]))


def row_add(request):
    """
    Добавление услуги
    """
    print 'here 113'
    if request.POST.get('id'):
        row = get_object_or_404(OrderRow, pk=request.POST.get('id'))
    else:
        row = OrderRow()
        row.order_id = request.POST.get('order')
    form = OrderRowForm(instance=row)
    print 120
    return render_to_response('order/ajax/row_add.html', locals(), context_instance=RequestContext(request))


def row_del(request):
    """
    Удаление услуги
    """
    row = OrderRow.objects.filter(id=request.POST.get('id'))
    if row:
        order = row[0].order
        row.delete()
        order.calc_sum()
        order.save()
    return HttpResponse("Ok")


@login_required
def order_save(request):
    return HttpResponse("")


def add_master_to_order(request, order_id, master_id):
    """
    Привязываем мастера к заказу
    """
    master = get_object_or_404(Person, pk=master_id)
    order = get_object_or_404(Order, pk=order_id)
    if order.master:
        res = "У этого заказа №%s уже есть мастер" % order.id
    else:
        res = "Мастер %s назначен к заказу №%s" % (master, order.id)
        order.master = master
        order.save()
    res += '<br> <a href="%s">К списку заказов</a>' % (reverse('order:order_list'))

    return HttpResponse(res)