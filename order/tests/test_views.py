__author__ = 'arhangel66'
# -- coding: utf-8 --
__author__ = 'arhangel66'
from django.test import TestCase, Client
from django_webtest import WebTest
from django.core.urlresolvers import reverse
from persons.models import Person
from order.models import Order, OrderRow
from order.tools import create_client_from_order


class PostFormTest(TestCase):
    def setUp(self):
        from django.contrib import auth
        from django.contrib.auth.models import User
        self.username = 'admin'
        self.password  = 'secret'
        self.user = User.objects.create_user(self.username, 'mail@example.com', self.password)
        self.user.is_staff = True
        self.user.is_superuser = True
        self.user.save()
        order = Order.objects.create(fio1=u"", fio2=u"", fio3=u"", phone='')
        person = Person.objects.create(fio1="Дербичев", fio2=u"Mihail", fio3=u"Borisovich", phone="79090706997")

        #self.user = auth.authenticate(username='arhangel66@yandex.ru', password='09081985')

        #self.posts = []
        #Person.objects.create(fio1=u'Дербичев')

    def test_order_add_available(self):
        c = Client()
        c.login(username=self.username, password=self.password)
        response = c.get(reverse('order:order_add'))
        self.assertEquals(response.status_code, 302)

    def test_order_add_available(self):
        c = Client()
        c.login(username=self.username, password=self.password)
        response = c.get(reverse('order:order_list'))
        self.assertEquals(response.status_code, 200)

    def test_order_edit_available(self):
        response = self.client.get(reverse('order:order_edit', args=[1]))
        self.assertEquals(response.status_code, 200)