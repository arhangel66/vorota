__author__ = 'arhangel66'
# -- coding: utf-8 --
__author__ = 'arhangel66'
from django.test import TestCase, Client
from django_webtest import WebTest
from django.core.urlresolvers import reverse
from persons.models import Person
from order.models import Order, OrderRow, CatalogUslug
from order.tools import create_client_from_order


class PostFormTest(TestCase):
    def setUp(self):
        order = Order.objects.create(fio1=u"", fio2=u"", fio3=u"", phone='')
        person = Person.objects.create(fio1="Дербичев", fio2=u"Mihail", fio3=u"Borisovich", phone="79090706997")
        pass
        #self.posts = []
        #Person.objects.create(fio1=u'Дербичев')

    def test_order_add_available(self):
        response = self.client.get(reverse('order:order_add'))
        self.assertEquals(response.status_code, 200)

    def test_add_client_to_order(self):
        """
        Добавляем клиента к заказу.
        - назначаем клиента
        - заменяем текущие параметры на те, что указаны у клиента
        - редиректим назад
        """
        # Генерируем адрес посылки
        order_id = 1
        person_id = 1
        url = reverse("order:add_client_to_order", args=[order_id, person_id])

        # Имитируем запрос
        response = self.client.get(url)

        # Проверяем, записались ли данные в ордер
        order = Order.objects.get(pk=order_id)
        person = Person.objects.get(pk=person_id)
        self.assertEquals(order.fio1, person.fio1)
        self.assertEquals(order.fio2, person.fio2)
        self.assertEquals(order.fio3, person.fio3)
        self.assertEquals(order.phone, person.phone)

        # Проверяем, получили ли редирект в конце
        self.assertEquals(response.status_code, 302)


class OrderRowTest(TestCase):
    """
    Проверка функционала создания, редактирования и удаления orders_row
    """
    def setUp(self):
        self.order = Order.objects.create(fio1=u"Дербичев2", fio2=u"Михаил", fio3=u"Борисович", phone='79090706997',)
        self.row = OrderRow.objects.create(order_id=self.order.id, item=1, price_sum=3000, price_mat=500, price_zp=500, price_trans=500)
        self.usluga = CatalogUslug.objects.create(name='teset')
        pass

    def test_create_order_row(self):
        c = Client()

        # Проверяю, что без указания row_id - идет пустая форма
        params = {'order': 1, 'id': ''}
        response = c.post(reverse('order:row_add'), params)
        self.assertEquals(response.status_code, 200)
        self.assertIn('row_id=""', response.content)

        # Проверяю, что при указании order_row - возвращается заполненная форма
        params = {'order': 1, 'id': '1'}
        response = c.post(reverse('order:row_add'), params)
        self.assertEquals(response.status_code, 200)
        self.assertIn('row_id="1"', response.content)

    def test_save_row_form(self):
        c = Client()
        params = {'order': '1', 'item': 1, 'price_sum': 2000, 'comment': 'asdf'}
        response = c.post(reverse('order:row_save'), params)

        self.assertEquals(response.status_code, 302)
        row = OrderRow.objects.get(pk=2)
        self.assertEquals(row.price_sum, 2000)
        order = Order.objects.get(id=1)
        self.assertEquals(order.sum_sum, 5000)

        # Меняю параметр на 3000, убеждаюсь, что он сохраняется
        params = {'order': '1', 'id': 2, 'item': 1, 'price_sum': 3000, 'comment': 'asdf'}
        response = c.post(reverse('order:row_save'), params)
        self.assertEquals(response.status_code, 302)
        row = OrderRow.objects.get(pk=2)
        self.assertEquals(row.price_sum, 3000)
        order = Order.objects.get(id=1)
        self.assertEquals(order.sum_sum, 6000)

    def test_del_row_form(self):
        c = Client()

        # Посылаю запрос на удаление первой строки и убеждаюсь, что она удаляется
        params = {'id': 1}

        order = Order.objects.get(id=1)
        self.order.calc_sum()
        self.assertEquals(self.order.sum_sum, 3000)
        response = c.post(reverse('order:row_del'), params)
        self.assertEquals(response.status_code, 200)
        row = OrderRow.objects.filter(pk=1)
        self.assertEquals(row.count(), 0)
        order = Order.objects.get(id=1)

        self.assertEquals(order.sum_sum, 0)

