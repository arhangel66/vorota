__author__ = 'arhangel66'
# -- coding: utf-8 --
__author__ = 'arhangel66'
from django.test import TestCase, Client
from django_webtest import WebTest
from django.core.urlresolvers import reverse
from persons.models import Person
from order.models import Order, OrderRow, Status, StatusHistory
from order.tools import create_client_from_order, search_orders_func, calc_sum_of_orders, save_status_to_history


class OrdersFunctionTest(TestCase):
    def setUp(self):
        client = Person.objects.create(fio1="Дербичев", fio2=u"Mihail", fio3=u"Borisovich", phone="79090706997")
        order = Order.objects.create(fio1=u"Дербичев2", fio2=u"Михаил", fio3=u"Борисович", phone='79090706997', client_id=client.id)

        order2 = Order.objects.create(fio1=u"1", fio2=u"2", fio3=u"3", phone='4', date='2013-11-01', client_id=client.id)
        order2.date = '2013-11-01'
        order2.save()
        self.order = order2

        OrderRow.objects.create(order_id=order.id, item=1, price_sum=3000, price_mat=500, price_zp=500, price_trans=500)
        OrderRow.objects.create(order_id=order.id, item=2, price_sum=5000, price_mat=1000, price_zp=500, price_trans=500)

        master = Person.objects.create(fio1="Мастер", fio2=u"Mihail", fio3=u"Borisovich", phone="79090706997", master=True, active=True)
        order3 = Order.objects.create(fio1=u"Васильев", fio2=u"123", fio3=u"123", phone='1231231', master_id=2, client_id=client.id)
        OrderRow.objects.create(order_id=order2.id, item=2, price_sum=5000, price_mat=1000, price_zp=500, price_trans=500)
        order.calc_sum()
        order2.calc_sum()
        order3.calc_sum()

    def test_order_model_create(self):
        order = Order.objects.create(fio1=u'Дербичев', phone='79090706997')
        self.order = order
        self.assertEquals(type(order.id), type(1))
        pass
        #self.assertEquals(response.status_code, 200)

    def test_order_row_model_create(self):
        order = Order.objects.get(id=1)
        row = OrderRow.objects.create(order_id=order.id, item=1, price_sum=1000)
        self.assertEquals(type(row.id), type(1))
        pass

    def test_order_recalc_function(self):
        """
        Должен считать сумму от элементов.
        """
        order = Order.objects.get(id=1)
        order.calc_sum()
        self.assertEquals(order.sum_sum, 8000)
        self.assertEquals(order.sum_mat, 1500)
        self.assertEquals(order.sum_trans, 1000)
        self.assertEquals(order.sum_zp, 1000)
        pass

    def test_create_user_from_order(self):
        """
        Создаем или находим пользователя от заказа.
        """
        order = Order.objects.get(id=1)
        client = create_client_from_order(order)
        self.assertEquals(type(client), Person)  # Мы что-то получили

        client_id = client.id
        client = create_client_from_order(order)
        self.assertEquals(client.id, client_id)  # При повторном обращении - ничего не создаем, т.к. находим пользователя по Ф + телефон

        order.phone = ''
        order.client = None
        client = create_client_from_order(order)
        self.assertNotEquals(client.id, client_id)  # Когда убрал телефон - уже создаем повторно

        client_id = client.id
        order.client = client
        client = create_client_from_order(order)
        self.assertEquals(client.id, client_id)  # Если был прикреплен клиент, то ниче не создаем

    def test_search_order(self):
        """
        Проверяю, работоспособность функции поиска списка заказов по дате, мастеру итд
        """
        date1 = '2013-10-31'
        date2 = '2013-11-02'
        master_id = 2

        # Проверка поиска по дате.
        orders = search_orders_func(date1=date1, date2=date2)
        self.assertEquals(orders[0].id, 2)

        # Поиск по мастеру
        orders = search_orders_func(master_id=2)
        self.assertEqual(orders[0].id, 3)

        # Совместный поиск
        orders = search_orders_func(date1=date1, date2=date2, master_id=2)
        self.assertEqual(orders.count(), 0)

    def test_calc_sum_of_orders(self):
        """
        Считает сумму заказов и количество заказов, создавая словарь от person_id
        """
        import datetime
        orders = Order.objects.all()
        person_dict = calc_sum_of_orders(orders)
        self.assertEqual(person_dict[1]['sum_sum'], 13000)
        self.assertEqual(person_dict[1]['count'], 3)
        #self.assertEqual(person_dict[1]['last_date'], self.order.date)


class OrderStatusTest(TestCase):
    def setUp(self):
        self.status1 = Status.objects.create(name="Заказ принят", sms_person=u'Спасибо за заказ', sms_master=u"Мастеру смс")
        self.status2 = Status.objects.create(name="Мастер назначен")
        self.status3 = Status.objects.create(name="Заказ завершен")
        self.status4 = Status.objects.create(name="Заказ отменен")
        self.client = Person.objects.create(fio1="Дербичев", fio2=u"Mihail", fio3=u"Borisovich", phone="79090706997")
        self.order = Order.objects.create(fio1=u"Дербичев2", fio2=u"Михаил", fio3=u"Борисович", phone='79090706997', client_id=self.client.id)
        self.order2 = Order.objects.create(fio1=u"1", fio2=u"2", fio3=u"3", phone='4', date='2013-11-01', client_id=self.client.id)
        self.status = Status.objects.create()
        self.order2.date = '2013-11-01'
        self.order2.save()

    def test_change_sttatus__save_to_history(self):
        save_status_to_history(self.status1, self.order)
        count = StatusHistory.objects.filter(order=self.order.id, status=self.status1.id).count()
        self.assertEqual(1, count)

    def test_dbl_change_sttatus__save_to_history_once(self):
        save_status_to_history(self.status1, self.order)
        save_status_to_history(self.status1, self.order)
        count = StatusHistory.objects.filter(order=self.order.id, status=self.status1.id).count()
        self.assertEqual(2, count)
