# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Person'
        db.create_table('persons_person', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fio1', self.gf('django.db.models.fields.CharField')(max_length=60, null=True, blank=True)),
            ('fio2', self.gf('django.db.models.fields.CharField')(max_length=60, null=True, blank=True)),
            ('fio3', self.gf('django.db.models.fields.CharField')(max_length=60, null=True, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('city', self.gf('django.db.models.fields.CharField')(default='\xd0\xa7\xd0\xb5\xd0\xbb\xd1\x8f\xd0\xb1\xd0\xb8\xd0\xbd\xd1\x81\xd0\xba', max_length=60, null=True, blank=True)),
            ('street', self.gf('django.db.models.fields.CharField')(max_length=60, null=True, blank=True)),
            ('apt', self.gf('django.db.models.fields.CharField')(max_length=6, null=True, blank=True)),
            ('master', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('persons', ['Person'])


    def backwards(self, orm):
        # Deleting model 'Person'
        db.delete_table('persons_person')


    models = {
        'persons.person': {
            'Meta': {'object_name': 'Person'},
            'apt': ('django.db.models.fields.CharField', [], {'max_length': '6', 'null': 'True', 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'default': "'\\xd0\\xa7\\xd0\\xb5\\xd0\\xbb\\xd1\\x8f\\xd0\\xb1\\xd0\\xb8\\xd0\\xbd\\xd1\\x81\\xd0\\xba'", 'max_length': '60', 'null': 'True', 'blank': 'True'}),
            'fio1': ('django.db.models.fields.CharField', [], {'max_length': '60', 'null': 'True', 'blank': 'True'}),
            'fio2': ('django.db.models.fields.CharField', [], {'max_length': '60', 'null': 'True', 'blank': 'True'}),
            'fio3': ('django.db.models.fields.CharField', [], {'max_length': '60', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'master': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '60', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['persons']