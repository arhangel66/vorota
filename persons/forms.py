# -*- coding: utf-8 -*-
from django import forms
from persons.models import Person
from persons.tools import fio_obrab, tel_obrab, obrab_multi_tel
from order.models import OrderType

class PersonForm(forms.Form):
    fio_full = forms.CharField(label="Полное ФИО", max_length=160, required=True, widget=forms.TextInput(attrs={'placeholder':'Полное ФИО'}),)
    orgname = forms.CharField(label="Название организации", max_length=160, required=False, widget=forms.TextInput(attrs={'placeholder':'Название организации'}),)
    email = forms.CharField(label="Email", max_length=160, required=False, widget=forms.TextInput(attrs={'placeholder':'Email'}),)
    id = forms.CharField(label="id", max_length=20, required=False, widget=forms.TextInput(attrs={'style': 'display:none'}))
    phone = forms.CharField(label="Телефон", max_length=100, required=False, widget=forms.TextInput(attrs={'placeholder':'Телефон'}))
    city = forms.CharField(label="Город", max_length=60, required=False, widget=forms.TextInput(attrs={'placeholder':'Город'}))
    subcity = forms.CharField(max_length=50, required=False, widget=forms.TextInput(attrs={'placeholder': "Населенный пункт"}))
    street = forms.CharField(label="Улица", max_length=60, required=False, widget=forms.TextInput(attrs={'placeholder':'Улица'}))
    home = forms.CharField(label="Улица", max_length=60, required=False, widget=forms.TextInput(attrs={'placeholder':'Дом'}))
    apt = forms.CharField(label="Квартира", max_length=6, required=False, widget=forms.TextInput(attrs={'placeholder':'Квартира'}))
    master = forms.BooleanField(label="Мастер?", required=False)
    active = forms.BooleanField(label="Активен?", required=False)
    comment = forms.CharField(widget=forms.Textarea(attrs={'cols': '50', 'rows': '4', 'class': 'span4'}), required=False)
    types = forms.ModelMultipleChoiceField(OrderType.objects.all(), widget=forms.SelectMultiple(attrs={'class': 'span3'}), required=False)

    def __init__(self, *args, **kwargs):
        person = kwargs.pop('person', None)
        super(PersonForm, self).__init__(*args, **kwargs)
        if person:
            self.initial["fio_full"] = person.get_fio
            self.initial["email"] = person.email
            self.initial["phone"] = person.phone
            self.initial["city"] = person.city
            self.initial["street"] = person.street
            self.initial["home"] = person.home
            self.initial["apt"] = person.apt
            self.initial["master"] = person.master
            self.initial["active"] = person.active
            self.initial["subcity"] = person.subcity
            self.initial["comment"] = person.comment
            self.initial["orgname"] = person.orgname
            self.initial["id"] = person.id
            self.fields['types'].initial = [c.pk for c in person.types.all()]


    def clean(self):
        super(PersonForm, self).clean()
        return self.cleaned_data

    def save(self):
        data = self.cleaned_data
        if data.get('id'):
            person = Person.objects.filter(id=data.get('id'))[0]
        else:
            person = Person()
        fio1, fio2, fio3 = fio_obrab(data['fio_full'])
        person.fio1 = fio1
        person.fio2 = fio2
        person.fio3 = fio3
        person.orgname = data['orgname']
        person.email = data['email']
        person.phone = obrab_multi_tel(data['phone'])
        person.subcity = data['subcity']
        person.city = data['city']
        person.street = data['street']
        person.home = data['home']
        person.apt = data['apt']
        person.comment = data['comment']
        person.master = data['master']
        person.active = data['active']
        person.save()
        person.types = data['types']

        person.save()

        return person


