__author__ = 'arhangel66'
# -*- coding: utf-8 -*-
from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('persons.views',
    # Examples:
    #url(r'^pool/(?P<idpool>\d+)/(?P<iduser>\w+)/(?P<idorder>\d+)/$', 'after_order', name="after_order",),
        url(r'^add/$', 'person_add', name="person_add",),
        url(r'^edit/(?P<person_id>\d+)/$', 'person_add', name="person_edit",),
        url(r'^list/$', 'person_list', name="person_list",),
        url(r'^master-list/$', 'master_list', name="master_list",),
        url(r'^ajax/person-search/$', 'person_search', name='person_search')
    )