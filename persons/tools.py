__author__ = 'arhangel66'
# -*- coding: utf-8 -*-
from persons.models import Person

import re
def fio_obrab(fio):
    """
    Обрабатываем ФИО получая Фамилия, Имя, отчество
    """

    fio = re.sub("[\.0-9]", " ", fio)
    if not fio:
        return '', '', ''
    f_name, i_name, o_name = '', '', ''
    words = (fio.split(' '))[:3]
    if len(words) > 0 and not 'n' in words[0]:
        f_name = words[0].rstrip()
    if len(words) > 1 and not 'n' in words[1]:
        i_name = words[1].rstrip()
    if len(words) > 2 and not 'n' in words[2]:
        o_name = words[2].rstrip()
    return f_name, i_name, o_name


def tel_obrab(tel):
    """
    Обрабатываем номер телефона, получая код и номер
    """
    kod = False
    numb = False
    if tel:
        if ',' in tel:
            tel = (tel.split(','))[0]
        tel = re.sub("[^0-9]", "", tel)
        if len(tel) > 11:
            tel = tel[:11]
        if len(tel) == 11: #89090706997
            kod = tel[1:4]
            numb = tel[4:]
        if len(tel) == 10: #9090706997
            kod = tel[0:3]
            numb = tel[3:]
        if len(tel) == 7: #7945703
            kod = False
            numb = tel
        if len(tel) == 3: #909
            kod = tel
        if kod and numb:
            return '7'+kod+numb
    return ''


def obrab_multi_tel(text):
    """
    Обработка нескольких телефонов
    """
    result = ''
    if ',' in text:
        for phone in text.split(','):
            result += tel_obrab(phone)+', '
    else:
        result += tel_obrab(text)+', '
    if len(result) > 3:
        result = result[:-2]
    return result

def find_person_func(full_fio='', phone='', orgname=''):
    """
    Ищет пользователей по фио и телефону, возвращает queryset
    """
    fio1, fio2, fio3 = fio_obrab(full_fio)
    persons = Person.objects.all()
    if fio1:
        persons = persons.filter(fio1__istartswith=fio1)
    if fio2:
        persons = persons.filter(fio2__istartswith=fio2)
    if fio3:
        persons = persons.filter(fio3__istartswith=fio3)

    if full_fio and not persons.count():
        persons = Person.objects.filter(orgname__icontains=full_fio)

    if orgname:
        persons = persons.filter(orgname__icontains=orgname)

    if phone:
        persons = persons.filter(phone__contains=phone)

    return persons

import datetime


def get_first_day_of_month():
    """
    Возвращает первую дату в месяце
    """
    days = int(datetime.datetime.now().strftime("%d"))-1
    date = datetime.date.today() - datetime.timedelta(days=days)
    return date