__author__ = 'arhangel66'
# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User



class Person(models.Model):
    fio1 = models.CharField(verbose_name="Фамилия", max_length=60, blank=True, null=True, default='')
    fio2 = models.CharField(verbose_name="Имя", max_length=60, blank=True, null=True, default='')
    fio3 = models.CharField(verbose_name="Отчество", max_length=60, blank=True, null=True, default='')
    orgname = models.CharField(verbose_name="Организация", max_length=60, blank=True, null=True, default='')
    phone = models.CharField(verbose_name="Телефон", max_length=100, blank=True, null=True, default='')
    email = models.CharField(verbose_name="Email", max_length=20, blank=True, null=True, default='')
    city = models.CharField(verbose_name="Город", max_length=60, blank=True, null=True, default='Челябинск')
    subcity = models.CharField(verbose_name="Населенный пункт", max_length=60, blank=True, null=True, default='')
    street = models.CharField(verbose_name="Улица", max_length=60, blank=True, null=True)
    comment = models.CharField(verbose_name="Комментарий", max_length=250, blank=True, null=True, default='')
    home = models.CharField(verbose_name="Дом", max_length=60, blank=True, null=True)
    apt = models.CharField(verbose_name="Квартира", max_length=6, blank=True, null=True)
    master = models.BooleanField(verbose_name="Мастер?", default=False)
    active = models.BooleanField(verbose_name="Активен?", default=False)
    types = models.ManyToManyField('order.OrderType', verbose_name=u"Типы заказов", blank=True, null=True)

    def __unicode__(self):
        if self.orgname:
            return u"%s, %s" % (self.orgname, self.get_main_name())
        return u"%s %s.%s." % (self.fio1, self.fio2[0] if self.fio2 else '', self.fio3[0] if self.fio3 else '')

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"

    def get_fio(self):
        return self.fio1 + ' ' + self.fio2 + ' ' + self.fio3

    def get_count_and_sum_orders(self):
        from order.models import Order
        if self.master:
            orders = Order.objects.filter(master=self.id)
        else:
            orders = Order.objects.filter(client=self.id)
        count = 0
        sum = 0
        for order in orders:
            count += 1
            sum += order.sum_sum
        return count, sum

    def get_phone(self):
        phone = self.phone
        if ',' in str(self.phone):
            phone = self.phone.split(',')[0]
        return phone

    def get_main_name(self):
        if self.fio1:
            return self.fio1
        elif self.fio2:
            return self.fio2
        else:
            return self.fio3

    def in_name(self):
        """
        Название в родительском падеже
        """
        if self.master:
            return "мастера"
        else:
            return "клиента"

    def other_name(self):
        if self.master:
            return "клиент"
        else:
            return "мастер"