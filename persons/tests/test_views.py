# -- coding: utf-8 --
__author__ = 'arhangel66'
from django.test import TestCase, Client
from django_webtest import WebTest
from django.core.urlresolvers import reverse
from persons.models import Person


class HomePageTest(TestCase):

    def setUp(self):
        self.posts = []
        Person.objects.create(fio1=u'Дербичев')

    def test_person_list_avaliable(self):
        c = Client()
        response = c.get(reverse('person:person_list', args=[]))
        self.assertEquals(response.status_code, 200)

    def test_person_add_avaliable(self):
        c = Client()
        response = c.get(reverse('person:person_add', args=[]))
        self.assertEquals(response.status_code, 200)

    def test_person_edit_avaliable(self):
        c = Client()
        response = c.get(reverse('person:person_edit', args=[1]))
        self.assertEquals(response.status_code, 200)
