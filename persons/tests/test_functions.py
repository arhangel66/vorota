# -- coding: utf-8 --
__author__ = 'arhangel66'
from django.test import TestCase, Client
from django_webtest import WebTest
from django.core.urlresolvers import reverse
from persons.models import Person
from persons.tools import find_person_func


class PersonSearch(TestCase):
    def setUp(self):
        self.posts = []
        Person.objects.create(fio1=u'Дербичев', phone="79090706997")
        Person.objects.create(fio1=u'Васильев', orgname='РДС', phone="79091234567")

    def test_person_search(self):
        person = find_person_func(full_fio="Дербичев")
        self.assertEquals(person[0].id, 1)

        person = find_person_func(phone="79091234567")
        self.assertEquals(person[0].id, 2)

        person = find_person_func(orgname="РДС")
        self.assertEquals(person[0].id, 2)