__author__ = 'arhangel66'
# -*- coding: utf-8 -*-

from django.contrib import admin
from persons.models import Person


class PersonAdmin(admin.ModelAdmin):
    list_display = ('fio1', 'fio2', 'active', 'master')
    list_filter = ('types', 'master')


admin.site.register(Person, PersonAdmin)