# -- coding: utf-8 --
from django.http import HttpResponseRedirect, Http404, HttpResponse,HttpResponseNotFound
from django.shortcuts import render_to_response,RequestContext, get_object_or_404
import datetime
from persons.forms import PersonForm
from persons.models import Person
from order.models import Order
from order.tools import calc_sum_of_orders, search_orders_func
from persons.tools import fio_obrab, find_person_func, get_first_day_of_month
from django.conf import settings

def person_add(request, person_id=False):
    """
    Добавить заказ
    """
    success = False
    if request.POST:
        form = PersonForm(request.POST)
        if form.is_valid():
            if form.save():
                if person_id:
                    success = "Успешно обновлен"
                else:
                    success = "Успешно создан"
                    form = PersonForm
    else:
        form = PersonForm
        if person_id:
            person = get_object_or_404(Person, id=person_id)
            form = PersonForm(person=person)
            if person.master:
                orders = Order.objects.filter(master=person.id)
            else:
                orders = Order.objects.filter(client=person.id)
    return render_to_response('person/person_add.html', locals(), context_instance=RequestContext(request))


def person_list(request):
    """
    Добавить заказ
    """
    persons = Person.objects.all()

    if request.POST:
        full_fio = request.POST.get('full_fio')
        phone = request.POST.get('phone')
        orgname = request.POST.get('orgname')
        persons = find_person_func(full_fio, phone, orgname)

    persons = persons.filter(master=False)
    persons_ids = persons.values_list('id', flat=True)
    orders = Order.objects.filter(client__in=persons_ids, status=settings.FINISH_STATUS_ID)
    person_dict = calc_sum_of_orders(orders)

    for person in persons:
        if person.id in person_dict:
            person.count = person_dict[person.id]['count']
            person.sum_sum = person_dict[person.id]['sum_sum']
            person.last_date = person_dict[person.id]['last_date']

    return render_to_response('person/person_list.html', locals(), context_instance=RequestContext(request))


def master_list(request):
    """
    Добавить заказ
    """
    persons = Person.objects.filter(master=True)

    date1 = request.POST.get('date1', get_first_day_of_month().strftime("%Y-%m-%d"))
    date2 = request.POST.get('date2', datetime.date.today().strftime("%Y-%m-%d"))
    finish = request.POST.get('finish', False)
    orders = search_orders_func(date1="%s 00:00:00" % date1, date2="%s 23:59" % date2, finish=finish)

    persons_ids = persons.values_list('id', flat=True)
    orders = orders.filter(master__in=persons_ids)
    person_dict = calc_sum_of_orders(orders, type='master')
    for person in persons:
        if person.id in person_dict:
            person.count = person_dict[person.id]['count']
            person.sum_sum = person_dict[person.id]['sum_sum']
            person.last_date = person_dict[person.id]['last_date']
    persons.order_by('active')
    return render_to_response('person/master_list.html', locals(), context_instance=RequestContext(request))


def person_search(request):
    """
    ajax поиск персона
    """
    if request.is_ajax():
        full_fio = request.POST.get('full_fio')
        phone = request.POST.get('phone')
        order_id = request.POST.get('order_id')
        persons = find_person_func(full_fio, phone)[:3]

        return render_to_response('person/ajax/quick_person_list.html', locals(), context_instance=RequestContext(request))
    return HttpResponseRedirect('/')


def auth(request):
    """
    Авторизация
    """
    from django.contrib import auth
    if request.POST.get('login'):
        user = auth.authenticate(username=request.POST.get('login'), password=request.POST.get('password'))
        if user is not None:
            auth.login(request, user)
            next = request.GET.get('next')
            if next:
                return HttpResponseRedirect(next)
            else:
                return HttpResponseRedirect('/')
        else:
            error = 'Логин или пароль неверны'
    return render_to_response('auth.html', locals(), context_instance=RequestContext(request))