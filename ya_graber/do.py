__author__ = 'arhangel66'
# -- coding: utf-8 --
import os, sys, re
path = os.path.normpath(os.path.join(os.getcwd(), '..'))
sys.path.append(path)

from django.core.management import setup_environ
import settings
setup_environ(settings)

from time import time
from pymorphy import get_morph
import re
import urllib, urllib2, cookielib
import lxml.html
import xlwt
from random import choice
from bs4 import BeautifulSoup
from xan.settings import MORPH_DICTS

# morph = get_morph('e:/www/xan/sqlite-json')

