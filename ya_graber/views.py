__author__ = 'arhangel66'
# -- coding: utf-8 --
import os, sys, re

path = os.path.normpath(os.path.join(os.getcwd(), '..'))
sys.path.append(path)
from django.http import HttpResponseRedirect, Http404, HttpResponse, HttpResponseNotFound
from django.shortcuts import render_to_response, RequestContext, get_object_or_404

from django.core.management import setup_environ
import settings

setup_environ(settings)

from time import time
from pymorphy import get_morph
import re
import urllib, urllib2, cookielib
import lxml.html
import xlwt
from random import choice
from bs4 import BeautifulSoup
from ya_graber.tools import anal_words, url_open, normalize_phrase, get_normal_word, url_open_fake
from django.contrib.auth.decorators import login_required
# morph = get_morph('e:/www/xan/sqlite-json')
from settings import MORPH_DICTS
# morph = get_morph('e:/www/xan/sqlite-json')

morph = get_morph(MORPH_DICTS)


def list_in_str(list):
    if not list:
        return ''
    str = ''
    for lis in list:
        str += "%s," % lis
    return str[:-1]

#@login_required
def get_list_words(request):
    """
    Основная страница
    """
    post = request.POST
    if post.get('words'):
        phrases = post.get('words')
        pages = post.get('deep')
        if not pages:
            pages = 1
        regions = post.get('regions')
        if not regions:
            regions = 56
        if not ',' in regions:
            regions += ','

        phrases = phrases.split('\n')
        tabler = []
        for phrase in phrases:
            if phrase:
                error = False
                bold_words = {}
                ish_phrase = phrase.strip()
                words_in_phrase = normalize_phrase(phrase) #Собираю нормализованные слова исходного запроса
                phrase = phrase.replace(' ', '+').strip().encode('utf-8')

                viv = []

                for region in regions.split(','):
                    if not region:
                        continue
                    for page in range(pages):
                        page += 1
                        bold_words, titles = get_bold_worlds(phrase, region, page, bold_words=bold_words)
                        for title in titles:
                            viv.append(title.contents)
                        # return HttpResponse(bold_words)
                        # viv += "%s" % (result)

                        # return HttpResponse(bold_words)

                col = ''
                # return HttpResponse(viv)
                # print bold_words
                for word, pos in bold_words.items():
                    if not word.lower() in words_in_phrase:
                        # col += "%s(%s) ; " % (word, list_in_str(pos).strip())
                        col += "%s ; " % word
                a = viv, '<hr>', col
                if error:
                    tabler.append([ish_phrase, 'Ошибка'])
                else:
                    tabler.append([ish_phrase, col.lower()])

    return render_to_response('yandex.html', locals(), context_instance=RequestContext(request))


def get_bold_worlds(phrase, region, page=1, bold_words={}):
    """
    Запрос к яндексу и обработка
    """
    try:
        dict = {'query': phrase, 'page': page, 'lr': region, 'sortby': 'rlv'}
        result = get_from_yandex_xml(dict)
        xml_soup = BeautifulSoup(result, 'xml')
        titles = xml_soup.response.findAll('title')
        urls = xml_soup.response.findAll('url')
        bold_words = anal_words(titles, bold_words, page)
        headlines = xml_soup.response.findAll('passage')
        bold_words = anal_words(headlines, bold_words, page)
    except:
        error = True
    for url in urls:
        print url.contents
    return bold_words, headlines


def get_yandex_pos(request):
    """
    Запрос к яндексу и обработка
    """
    import time
    import datetime
    from ya_graber.views import get_from_yandex_xml

    FUNCTION_SERIAL = 'get_yandez_pos'
    ret = {'date': '', 'error': False}
    try:
        phrase = ''
        dict = {'query': phrase, 'page': 1, 'lr': 56, 'sortby': 'tm'}
        result = get_from_yandex_xml(dict)
        xml_soup = BeautifulSoup(result, 'xml')
        found = xml_soup.response.findAll('found-human')
        date = xml_soup.response.findAll('modtime')
        dateformat = ''
        if found:
            dat = "%s" % str(date[0].contents[0])
            dateformat = '%s-%s-%s' % (dat[0:4], dat[4:6],  dat[6:8])
            ret['date'] = dateformat
    except:
        ret['error'] = '%s unnown error' % FUNCTION_SERIAL

    return ret

def get_from_yandex_xml(dict):
    arh = 'http://xmlsearch.yandex.ru/xmlsearch?user=arhangel66&key=03.26979689:38309911b6c26429758a679e0c971702'
    xan = 'http://xmlsearch.yandex.ru/xmlsearch?user=llic-xan-ru&key=03.228437961:769cf4ce401e5aa195dd75e1dac81bf8&maxpassages=3'

    group = 'groupby=[attr%3D.mode%flat.groups-on-page%3D50.docs-in-group%3D1]'
    # group = ''
    url = "%s&%s&%s" % (xan, urllib.urlencode(
            dict), group)
    result = url_open_fake(url)
    return result

