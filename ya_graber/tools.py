__author__ = 'arhangel66'
# -- coding: utf-8 --
import os, sys, re
path = os.path.normpath(os.path.join(os.getcwd(), '..'))
sys.path.append(path)

from django.core.management import setup_environ
import settings
setup_environ(settings)

from time import time
from pymorphy import get_morph
import re
import urllib, urllib2, cookielib
import lxml.html
import xlwt
from random import choice
from bs4 import BeautifulSoup
from settings import MORPH_DICTS

# morph = get_morph('e:/www/xan/sqlite-json')
morph = get_morph(MORPH_DICTS)


def get_normal_word(word):
    """
    Возвращает нормальное слово
    """
    try:
        word = word.decode('utf-8')
    except:
        pass
    word = u"%s" % (word)
    norm_words = morph.normalize((word).upper())
    if norm_words:
        if type(norm_words) == set:
            for norm_word in norm_words:
                return norm_word
        else:
            return norm_words


def normalize_phrase(phrase):
    """
    Берет фразу и приводит её к "Нормальному виду".
    """
    p = re.compile('[,.-:!&?]*', re.U)
    phrase = p.sub( '', phrase).strip()
    p = re.compile('[\s]*', re.U)
    words = p.split(phrase)
    all_norm_words = []
    for word in words:
        word = get_normal_word(word)
        if word:
            if not word in all_norm_words:
                all_norm_words.append(word.lower())

    return all_norm_words



#zapros = normalize_phrase(u'Архангел, тебе нужно срочно разработать положение о прокурорах, стать ген. прокурором и гонять СМов. ')

"""
import socket
true_socket = socket.socket
def bound_socket(*a, **k):
    sock = true_socket(*a, **k)
    sock.bind(('217.107.34.195', 0))
    return sock
socket.socket = bound_socket
"""

def url_open_fake(url):
    """
        Функция открытия урла для парсинга

    """
    req = urllib2.Request(url)
    response = urllib2.urlopen(req, timeout=30)
    resp = response.read()
    return resp


def url_open(url):
    """
        Функция открытия урла для парсинга

    """
    socket = False
    del(socket)
    import socket
    #try:
    user_agents = [
        'Mozilla/5.0 (Windows; U; Windows NT 5.1; it; rv:1.8.1.11) Gecko/20071127 Firefox/2.0.0.11',
        'Opera/9.25 (Windows NT 5.1; U; en)',
        'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)',
        'Mozilla/5.0 (compatible; Konqueror/3.5; Linux) KHTML/3.5.5 (like Gecko) (Kubuntu)',
        'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.12) Gecko/20070731 Ubuntu/dapper-security Firefox/1.5.0.12',
        'Lynx/2.8.5rel.1 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/1.2.9'
    ]
    headers = {'User-Agent' : choice(user_agents) }
    values = {'name': 'Michael Frankorf',
              'location': 'Google LTD',
              'language': 'en-US' }
    data = None #urllib.urlencode(values)
    req = urllib2.Request(url, data, headers)
    req.add_header('Referer', 'http://www.google.com/')
    req.add_header('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')
    req.add_header('Accept-Language', 'ru-ru,ru;q=0.8,en-us;q=0.5,en;q=0.3')
    req.add_header('Content-Type', 'application/x-www-form-urlencoded')
    req.add_header('Connection', 'Keep-Alive')
    req.add_header('Accept-Charset', 'windows-1251,utf-8;q=0.7,*;q=0.7')
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookielib.CookieJar()))
    urllib2.install_opener(opener)
    try:
        response = urllib2.urlopen(req, timeout=30)
    except socket.timeout as e:
        #print type(e)    #catched
        # For Python 2.7
        return "There was an error: %r" % e
    #file_object = open("thefile.html", 'w')
    #file_object.write(response.read())
    #file_object.close()
    resp = response.read()
    if '</head>' in resp:
        head = resp.split('</head>')[0]
    elif '<body>' in resp:
        head = resp.split('<body>')[0]
    else:
        head = resp[:1000]

    import re
    p = re.compile('charset=(.*)"', re.U)
    charset = p.findall(head)
    if charset:
        head = charset[0]
        #print '112: %s' % head

    dop_find = ''
    try:
        if 'utf-8' in head or 'utf8' in head:
            resp = resp.decode('UTF-8')
            #print 'utf-8'
        elif 'windows-1251' in head or 'cp1251' in head:
            resp = resp.decode('windows-1251')
            #print 'windows-1251'
        elif 'KOI8-R' in head:
            resp = resp.decode('KOI8-R')
            #print 'koir-8'
        else:
            dop_find = True
    except:
        print 'err  decode from head in url_open'
        dop_find = True

    if dop_find:
        # Если не удалось найти упоминания кодировки - пробуем всё подряд
        try:
            resp = resp.decode('UTF-8')
        except:
            try:
                resp = resp.decode('windows-1251')
            except:
                try:
                    resp = resp.decode('KOI8-R')
                except:
                    pass
    if not resp:
        print 'err decode in url_open2'

    p = re.compile("<\?xml(.*)\?>", re.U)
    resp = p.sub('', resp)
    return resp

    #except:
    #    print "fal"
    #    return False



def anal_words(titles, boldwords, page):
    """
    Анализирует ветку xml
    """
    i = 0
    for title in titles:
        i += 1
        hlwords = title.findAll('hlword')
        if hlwords:
            for hlword in hlwords:
                hlword = get_normal_word(("%s" % hlword).replace('<hlword>', '').replace('</hlword>', ''))
                pos = i + (page-1)*10
                if not hlword in boldwords:
                    boldwords[hlword.lower()] = [pos]
                else:
                    boldwords[hlword.lower()].append(pos)
    return boldwords

