# -*- coding: utf-8 -*-
from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
import settings

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'order.views.order_list', name='home'),
    # url(r'^forum/showthread.php\?t=(?P<subj_id>\d+)&page=(?P<page_id>\d+)', 'views.home', name='subject'),

    # url(r'^vg/', include('foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^accounts/login/', 'persons.views.auth', name='auth'),
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    #url(r'^view/$', 'views.view', name='view'),
    #url(r'^yandex/', 'ya_graber.views.get_list_words', name='get_list_words'),
    #url(r'^yandex_ind/', 'ya_graber.views.get_yandex_pos', name='get_yandex_pos'),
    #url(r'^accounts/login/', 'articles.views.auth', name='auth'),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
)





# analize articles
urlpatterns += patterns('',
    url(r'^order/', include('order.urls', namespace='order')),
)

# analize articles
urlpatterns += patterns('',
    url(r'^person/', include('persons.urls', namespace='person')),
)
