__author__ = 'arhangel66'
# -*- coding: utf-8 -*-

from django.db import models
#from tinymce import models as tinymce_models
from django.core.urlresolvers import reverse

class Person(models.Model):
    name            = models.CharField(verbose_name="Название", max_length = 255)
    number          = models.IntegerField(verbose_name="id на форуме")
    moderator       = models.BooleanField(verbose_name="Модератор?", default=False, )

    def __unicode__(self):
        return u"%s" % self.name

    def count_mess(self):
        count = Post.objects.filter(author = self).count()
        return count

    class Meta:
        verbose_name = u"Пользователь"
        verbose_name_plural = u"Пользователи"


class Group(models.Model):
    """
    Раздел
    """
    name            = models.CharField(verbose_name="Название", max_length=255,)
    number          = models.IntegerField(verbose_name="id на форуме")
    moderator       = models.ForeignKey(Person, verbose_name="Модератор", null=True, blank=True)

    def __unicode__(self):
        return u"Раздел: %s" % self.name

    class Meta:
        verbose_name = u"Раздел"
        verbose_name_plural = u"Разделы"


class Subject(models.Model):
    """
    Тема
    """
    name            = models.CharField(verbose_name="Название", null=True, blank=True, max_length = 255)
    number          = models.IntegerField(verbose_name="id на форуме", null=True, blank=True)
    date_create     = models.DateTimeField(verbose_name="Дата создания", null=True, blank=True)
    author          = models.ForeignKey(Person, verbose_name="Создатель", null=True, blank=True)
    last_page       = models.IntegerField(verbose_name="Последняя прочитанная страница", default=1)
    group           = models.ForeignKey(Group, verbose_name="Раздел", null=True, blank=True)


    def __unicode__(self):
        return u"Тема: %s" % self.name

    class Meta:
        verbose_name = u"Тема"
        verbose_name_plural = u"Темы"


class Post(models.Model):
    """
    Сообщение
    """
    number          = models.IntegerField(verbose_name="Номер сообщения")
    date_create     = models.DateTimeField(verbose_name="Дата создания")
    author          = models.ForeignKey(Person, verbose_name="Создатель")
    subject         = models.ForeignKey(Subject, verbose_name="Тема")

    def __unicode__(self):
        return u"m: %s - %s" % (self.author, self.number)

    class Meta:
        verbose_name = u"Сообщение"
        verbose_name_plural = u"Сообщения"



        # Раздел id   название
        # Тема   id   название   дата создания  раздел  количество сообщений    автор
        # Пользователь  id ник

        # Сообщение
        # - тема
        # - пользователь
        # - дата
        #