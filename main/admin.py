__author__ = 'arhangel66'
# -*- coding: utf-8 -*-

from django.contrib import admin
from main.models import Group, Post, Person, Subject

class MultiDBModelAdmin(admin.ModelAdmin):
    # A handy constant for the name of the alternate database.
    using = 'master'

    def save_model(self, request, obj, form, change):
        # Tell Django to save objects to the 'other' database.
        obj.save(using=self.using)

    def delete_model(self, request, obj):
        # Tell Django to delete objects from the 'other' database
        obj.delete(using=self.using)

    def queryset(self, request):
        # Tell Django to look for objects on the 'other' database.
        return super(MultiDBModelAdmin, self).queryset(request).using(self.using)

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        # Tell Django to populate ForeignKey widgets using a query
        # on the 'other' database.
        return super(MultiDBModelAdmin, self).formfield_for_foreignkey(db_field, request=request, using=self.using, **kwargs)

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        # Tell Django to populate ManyToMany widgets using a query
        # on the 'other' database.
        return super(MultiDBModelAdmin, self).formfield_for_manytomany(db_field, request=request, using=self.using, **kwargs)


class GroupAdmin(admin.ModelAdmin):
    list_display = ('name', 'number', 'moderator' )
    list_display_links = ('name',)
    search_fields = ['name', 'number']


class PostAdmin(admin.ModelAdmin):
    list_display = ('number','author', 'subject', 'date_create' )
    list_display_links = ('number',)
    search_fields = ['number', 'subject',]
    ordering = ['-date_create',]
    list_filter = ('subject__group',  )

class SubjectAdmin(admin.ModelAdmin):
    list_display        = ('number','name', 'date_create', 'group')
    list_display_links  = ('name',)
    search_fields       = ('number', 'name',)
    ordering = ['-date_create',]
    list_filter = ('group',  )


class PersonAdmin(admin.ModelAdmin):
    list_display = ('name', 'number', 'moderator', 'count_mess')
    list_display_links = ('name',)
    list_filter = ('group', )
    search_fields = ['name', 'number' ]


# admin.site.register(Person, PersonAdmin)
# admin.site.register(Subject, SubjectAdmin)
# admin.site.register(Post, PostAdmin)
# admin.site.register(Group, GroupAdmin)
