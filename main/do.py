# -*- coding: utf-8 -*-


__author__ = 'arhangel66'
import os, sys, re
import lxml.html
path = os.path.normpath(os.path.join(os.getcwd(), '..'))
sys.path.append(path)

from django.core.management import setup_environ
import settings

setup_environ(settings)

# from order.tools import send_sms
# print send_sms(phone='79090706997', text='Тест')

# from order.tools import create_sms_text, send_sms
# from order.models import StatusHistory, Person
# master = Person.objects.filter(master=True)[0]
# status_history = StatusHistory.objects.get(id=31)
# text_sms = create_sms_text(status_history.status.sms_master, status_history.order)
# print text_sms
# status_history.sms_master = text_sms
# send_sms(text=text_sms, phone=master.phone)
from order.models import Order
from persons.models import Person
Order.objects.filter(fio1='False').update(fio1='')
Order.objects.filter(fio2='False').update(fio2='')
Order.objects.filter(fio3='False').update(fio3='')
Person.objects.filter(fio1='False').update(fio1='')
Person.objects.filter(fio2='False').update(fio2='')
Person.objects.filter(fio3='False').update(fio3='')

from persons.models import Person
Person.objects.all().update(active=True)
from persons.tools import fio_obrab
print "|".join(fio_obrab('Дербичев Михаил Борисович'))
print "|".join(fio_obrab(' Михаил Борисович'))
print "|".join(fio_obrab('  Борисович'))
print "|".join(fio_obrab('Дербичев  Борисович'))